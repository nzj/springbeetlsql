package com.park.oss.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.ext.spring.SpringBeetlSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.park.oss.model.Park;
import com.park.oss.model.SysUser;
import com.park.oss.service.ParkSearchService;
import com.park.oss.vo.FreePark;

@Service
public class ParkSearchServiceImpl implements ParkSearchService {

	@Autowired
	SQLManager sqlManager ;
	@Override
	public List<Park> search(Double lon, Double lat) {
		Map paras = new HashMap();
		paras.put("lat", lat);
		paras.put("lon", lon);
		return sqlManager.select("park.search", Park.class, paras);
	}
	@Override
	public List<FreePark> searchFreePark(SysUser user,Double lon,Double lat, String day) {
		Map paras = new HashMap();
		paras.put("lat", lat);
		paras.put("lon", lon);
		paras.put("day", day);
		paras.put("userId", user.getId());
		return sqlManager.select("park.searchFree", FreePark.class, paras);

	}

}
