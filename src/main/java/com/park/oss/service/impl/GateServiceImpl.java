package com.park.oss.service.impl;

import java.util.List;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.ext.spring.SpringBeetlSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.park.oss.model.Gate;
import com.park.oss.service.GateService;

@Service
@Transactional
public class GateServiceImpl implements GateService {
	@Autowired
	SQLManager sqlManager ;
	@Override
	public void addGate(Gate gate) {
		sqlManager.insert(gate);
	}

	@Override
	public List<Gate> getGates(Integer parkId) {
		Gate query = new Gate();
		query.setParkId(parkId);
		return sqlManager.template(query);
	}

	@Override
	public void updateGate(Gate gate) {
		 sqlManager.updateById(gate);

	}

	@Override
	public void removeGate(Integer id) {
		 sqlManager.deleteById(Gate.class, id);

	}

	@Override
	public Gate getGate(Integer id) {
		return  sqlManager.unique(Gate.class, id);
	}

}
