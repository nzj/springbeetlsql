package com.park.oss.service;

import java.util.List;

import com.park.oss.model.CarPlace;
import com.park.oss.model.PlaceRent;
import com.park.oss.model.PlaceRentRule;
import com.park.oss.vo.CarPlaceVo;

public interface CarPlaceService {
	public List<CarPlaceVo> query(Integer parkId,String plateNumber,String moble);
	public void addPlaceRent(PlaceRent rent);
	public void addPlaceRentRule(PlaceRentRule rentRule);
	public CarPlace getCarPlace(Long id);
}
