package com.park.oss.vo;

import com.park.oss.model.CarPlace;

public class CarPlaceVo extends CarPlace {

	private Double amount ;
	private String parkName;
	private String placeName;

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getParkName() {
		return parkName;
	}

	public void setParkName(String parkName) {
		this.parkName = parkName;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	
}
