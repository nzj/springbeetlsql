package com.park.oss.vo;

import com.park.oss.model.Park;

/** 剩余车位
 * @author xiandafu
 *
 */
public class ParkSearchVo  extends Park{
	private int totalPlace;
	private int balancePlace;
	private double distance;
	public int getTotalPlace() {
		return totalPlace;
	}
	public void setTotalPlace(int totalPlace) {
		this.totalPlace = totalPlace;
	}
	public int getBalancePlace() {
		return balancePlace;
	}
	public void setBalancePlace(int balancePlace) {
		this.balancePlace = balancePlace;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
}
