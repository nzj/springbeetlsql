package com.park.oss.web.console;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.park.oss.model.Payment;
import com.park.oss.service.ParkService;
import com.park.oss.service.PaymentService;

@Controller
@RequestMapping("/payment")
public class PaymentController {
	@Autowired
	PaymentService service ;
	
	@Autowired
	ParkService parkService ;
    @RequestMapping(value = "/park.html", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(value="parkId", required=true)  Integer parkId) {
    	ModelAndView view = new ModelAndView("/payment/park.btl");
    	List<Payment> list = service.query(parkId);
    	view.addObject("list",list);
    	view.addObject("park",parkService.getPark(parkId));
        return view;
    }
}
